extends PlayerState

func enter(player:KinematicBody2D) -> void:
	player.play_animation("running")
	player.jump_count = 0

func exit(player:KinematicBody2D) -> void:
	pass

func update(player:KinematicBody2D, delta:float) -> void:
	var desired_movement = player.get_movement()
	
	if desired_movement.x == 0:
		player.velocity.x = 0
		player.change_state("idle")
		return
	
	handle_slippery_blocks(player, desired_movement)
	handle_moving_blocks(player)
	player.cap_vertical_speed()
	if handle_input(player):
		return
	
	player.handle_flip(desired_movement.x)
	player.velocity.x *= desired_movement.x
	
	player.apply_gravity()
	
	player.velocity = player.move_and_slide_with_snap(player.velocity, Vector2(0, 3), -player.gravity_direction, true, 4, player.max_slope, false)
	
	if player.check_for_death():
		player.kill()
	
	if not player.is_on_floor():
		player.jump_count += 1
		player.change_state("fall")
	

func handle_input(player:KinematicBody2D) -> bool:
	if Input.is_action_pressed(player.get_input_shoot()):
		player.shoot()
	
	var touching = player.is_touching_platform()
	if (touching or player.can_jump()) and Input.is_action_just_pressed(player.get_input_jump()):
		if touching:
			player.jump_count = 0
		player.change_state("jump")
		return true
	
	if Input.is_action_just_pressed(player.get_input_suicide()):
		player.kill()
	
	return false
