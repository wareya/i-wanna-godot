extends Camera2D

export var target_player:int = 1

var our_player:Player

# Called when the node enters the scene tree for the first time.
func _ready():
	var players = get_tree().get_nodes_in_group("player")
	for player in players:
		if player.player_num == target_player:
			our_player = player

func _physics_process(delta):
	if our_player:
		position = our_player.position
