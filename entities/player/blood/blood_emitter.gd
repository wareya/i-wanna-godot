extends Node2D

export var amount = 40

export var velocity = Vector2(-1000, 1000)

onready var bloods = [
	preload("blood_0.tscn"),
	preload("blood_1.tscn"),
	preload("blood_2.tscn")
]

# Called when the node enters the scene tree for the first time.
func _ready():
	yield(get_tree(),"idle_frame")
	for x in range(0, amount):
		var blood:RigidBody2D = bloods[floor(rand_range(0, bloods.size()))].instance()
		get_tree().current_scene.add_child(blood)
		
		blood.global_position = global_position
		
		if randf() < 0.5:
			blood.apply_impulse(Vector2(0,0), Vector2(velocity.x, 5000)) 
		else:
			blood.apply_impulse(Vector2(0,0), Vector2(velocity.y, 5000)) 
		
	
	queue_free()
