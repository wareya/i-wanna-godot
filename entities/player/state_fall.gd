extends PlayerState

func enter(player:KinematicBody2D) -> void:
	player.play_animation("fall")

func exit(player:KinematicBody2D) -> void:
	pass

func update(player:KinematicBody2D, delta:float) -> void:
	var desired_movement = player.get_movement()
	player.handle_flip(desired_movement.x)
	
	handle_normal_movement(player)
	player.velocity.x *= desired_movement.x
	
	if not player.is_upside_down and player.velocity.y < 0:
		player.velocity.y *= 0.45
	elif player.is_upside_down and player.velocity.y > 0:
		player.velocity.y *= 0.45
	
	player.apply_gravity()
	player.cap_vertical_speed()
	if handle_input(player):
		return
	
	player.velocity = player.move_and_slide_with_snap(player.velocity, Vector2(0, 3), -player.gravity_direction, true, 4, player.max_slope, false)

	if player.check_for_death():
		player.kill()

	if player.is_on_floor():
		if player.velocity.x == 0:
			player.change_state("idle")
		else:
			player.change_state("running")

func handle_input(player:KinematicBody2D) -> bool:
	if Input.is_action_pressed(player.get_input_shoot()):
		player.shoot()
	
	var touching = player.is_touching_platform()
	if (touching or player.can_jump()) and Input.is_action_just_pressed(player.get_input_jump()):
		if touching:
			player.jump_count = 0
		player.change_state("jump")
		return true
	
	if Input.is_action_just_pressed(player.get_input_suicide()):
		player.kill()
	
	return false
