extends Node2D

onready var player = get_parent()

func _physics_process(delta):
	$VBoxContainer/Name.text = "%s" % [player.player_name]
	#$VBoxContainer/Position.text = "x: %.3f | y: %.3f" % [player.position.x, player.position.y]
	$VBoxContainer/OnThings.text = "P(%s) F(%s) W(%s) C(%s)" % [player.is_touching_platform(), player.is_on_floor(), player.is_on_wall(), player.is_on_ceiling()]
	$VBoxContainer/Jumps.text = "Jumps: %s / %s (inf: %s)" % [player.jump_count, player.jump_max, player.is_infinite_jump]
	$VBoxContainer/State.text = "State: %s" % [player.current_state]
	$VBoxContainer/Speed.text = "r: %s" % [player.rotation_degrees]
