extends PlayerState

func enter(player:KinematicBody2D) -> void:
	player.play_animation("idle")
	player.jump_count = 0

func exit(player:KinematicBody2D) -> void:
	pass

func update(player:KinematicBody2D, delta:float) -> void:
	var desired_movement = player.get_movement()
	
	player.handle_flip(desired_movement.x)
	
	if desired_movement.x != 0:
		player.change_state("running")
	
	handle_slippery_blocks(player, desired_movement)
	handle_moving_blocks(player)
	player.apply_gravity()
	player.cap_speed()
	if handle_input(player):
		return

	player.velocity = player.move_and_slide_with_snap(player.velocity, Vector2(0, 3), -player.gravity_direction, true, 4, player.max_slope, false)

	if player.check_for_death():
		player.kill()
		
	if not player.is_on_floor():
		player.jump_count += 1
		player.change_state("fall")

func handle_input(player:KinematicBody2D) -> bool:
	if Input.is_action_pressed(player.get_input_shoot()):
		player.shoot()
	
	var touching = player.is_touching_platform()
	if (touching or player.can_jump()) and Input.is_action_just_pressed(player.get_input_jump()):
		if touching:
			player.jump_count = 0
		player.change_state("jump")
		return true
	
	if Input.is_action_just_pressed(player.get_input_suicide()):
		player.kill()
	
	return false

func handle_slippery_blocks(player:KinematicBody2D, movement:Vector2) -> void:
#	if player.is_on_floor():
#		var found_slip:bool = false
#		for i in player.get_slide_count():
#			var collision = player.get_slide_collision(i)
#			if "physics_material_override" in collision.collider: #touching_slippery_block
#				# if the player is on a slippery block
#				# move the player towards 0 horizontal speed using the slip amount as "friction"
#				# if the player reaches 0 or lower, set it to 0
#				player.velocity.x += collision.collider.physics_material_override.friction * movement.x * player.speed
#				if abs(player.velocity.x) <= 0:
#					player.velocity.x = 0
#				found_slip = true
#				break
#		if not found_slip:
#			# if the player is not touching a slippery block
#			# immediately set the horizontal speed to 0
	player.velocity.x = 0

func handle_moving_blocks(player:KinematicBody2D):
	# i'll be honest i don't know why the kid moves 1 pixel every second or so
	if player.is_on_floor():
		player.velocity = player.get_floor_velocity()/player.framecontrol
