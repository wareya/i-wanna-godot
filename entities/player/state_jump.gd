extends PlayerState

var jump_frames:int = 0
var start_pos:Vector2

func enter(player:KinematicBody2D) -> void:
	jump_frames = 0
	start_pos = player.global_position
	player.jump_count += 1
	player.play_animation("jump")
	
	var mod = 1
	if player.is_upside_down:
		mod = -1
	
	if player.jump_count == 1:
		player.play_sound("jump")
		player.velocity.y = -8.5 * player.framecontrol * mod
	else:
		player.play_sound("jump_double")
		player.velocity.y = -7 * player.framecontrol * mod

func exit(player:KinematicBody2D) -> void:
	#printt("jump frames:", jump_frames)
	#printt("distance_travelled", player.global_position - start_pos)
	pass

func update(player:KinematicBody2D, delta:float) -> void:
	var desired_movement = player.get_movement()
	player.handle_flip(desired_movement.x)

	handle_normal_movement(player)
	player.velocity.x *= desired_movement.x
	player.apply_gravity()
	player.cap_vertical_speed()
	if handle_input(player):
		return
	
	player.velocity = player.move_and_slide_with_snap(player.velocity, Vector2(0, 0), -player.gravity_direction, true, 4, player.max_slope, false)
	jump_frames += 1
	
	if player.check_for_death():
		player.kill()
	
	if player.is_on_floor():
		player.change_state("idle")
	elif not Input.is_action_pressed(player.get_input_jump()):
		player.change_state("fall")
	elif not player.is_upside_down and player.velocity.y >= 0:
		player.change_state("fall")
	elif player.is_upside_down and player.velocity.y <= 0:
		player.change_state("fall")

func handle_input(player:KinematicBody2D) -> bool:
	if Input.is_action_pressed(player.get_input_shoot()):
		player.shoot()
	if Input.is_action_just_pressed(player.get_input_suicide()):
		player.kill()
	
	return false
