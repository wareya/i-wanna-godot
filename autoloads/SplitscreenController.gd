extends Control

export var num_players:int = 1
var max_players:int = 4

onready var grid = $GridContainer

enum layout {
	horizontal_stacked,
	vertical_stacked,
	four_by_four
}
export(layout) var current_layout = layout.four_by_four

func _ready():
	update_viewports()

func set_players(num:int):
	if num <= max_players or num > 0:
		num_players = num
		update_viewports()
	else:
		printerr("Tried to set players outside of range")

func update_viewports():
	# update how many players should be visible
	
	for idx in range(0, grid.get_child_count()):
		var child:Control = grid.get_child(idx)
		child.visible = (idx < num_players)
		var viewport:Viewport = child.get_child(0)
	
	if current_layout == layout.vertical_stacked:
		grid.columns = num_players
	elif current_layout == layout.horizontal_stacked:
		grid.columns = 1
	else:
		grid.columns = 2
		if num_players == 1:
			grid.columns = 1
	
