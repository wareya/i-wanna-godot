extends Node2D
class_name Destroyable

var _explosion_scene = preload("res://objects/explosion/explosion.tscn")

func _exit_tree():
	var explosion = _explosion_scene.instance()
	explosion.global_position = global_position
	get_tree().current_scene.call_deferred("add_child", explosion)
