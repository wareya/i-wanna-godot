extends Area2D

export(String, FILE, "*.tscn") var next_map

func _on_Warp_body_entered(body):
	if body is Player:
		if next_map:
			get_tree().change_scene(next_map)
