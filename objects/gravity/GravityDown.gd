extends Area2D

func _on_GravityDown_body_entered(body):
	if body.has_method("flip_vertical"):
		body.flip_vertical(true)
