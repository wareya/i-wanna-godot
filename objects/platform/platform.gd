extends KinematicBody2D

func _on_Area2D_body_entered(body):
	if "is_in_platform" in body:
		body.is_in_platform = true

func _on_Area2D_body_exited(body):
	if "is_in_platform" in body:
		body.is_in_platform = false
