extends KinematicBody2D

#whether or not the button is currently pressed
export var is_pressed:bool = false
# if this is set, the button will unpress itself after this amount of time
export(float, 0, 3600) var timeout_sec:float = 0.0

onready var anim = $AnimationPlayer

signal pressed(player)
signal unpressed()

func _ready() -> void:
	if is_pressed:
		press()

func get_shot(player:Player = null) -> void:
	if not is_pressed:
		press(player)
	
func press(player:Player = null) -> void:
	is_pressed = true
	anim.play("press")
	emit_signal("pressed", player)
	
	if timeout_sec > 0:
		get_tree().create_timer(timeout_sec).connect("timeout", self, "unpress")

func unpress() -> void:
	is_pressed = false
	anim.play("unpress")
	emit_signal("unpressed")
